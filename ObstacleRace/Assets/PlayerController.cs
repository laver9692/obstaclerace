﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーにアタッチするコンポーネント
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    /// <summary>自分自身に追加されている Rigidbody への参照</summary>
    Rigidbody m_rb;
    /// <summary>前進する力</summary>
    [SerializeField] float m_movePower = 1.0f;
    /// <summary>ジャンプする力</summary>
    [SerializeField] float m_jumpPower = 1.0f;
    /// <summary>生成時に勝手に前進するかどうか</summary>
    [SerializeField] bool m_workOnStart = false;
    /// <summary>自分を写しているカメラへの参照</summary>
    [SerializeField] Camera m_camera;
    /// <summary>接地判定</summary>
    bool m_isGrounded = false;
    /// <summary>動いているかどうか</summary>
    bool m_isWorking = false;
    /// <summary>タイマー</summary>
    float m_timer;

    void Start()
    {
        m_rb = GetComponent<Rigidbody>();

        if (m_workOnStart)
        {
            StartWorking();
        }
    }

    void FixedUpdate()
    {
        if (!m_isWorking) return;

        m_timer += Time.fixedDeltaTime; // ラップタイムを計測する
        m_rb.AddForce(Vector3.forward * m_movePower, ForceMode.Acceleration);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (m_isGrounded)
            {
                m_isGrounded = false;
                m_rb.AddForce(Vector3.up * m_jumpPower, ForceMode.Impulse);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            m_isGrounded = true;
        }
        else if (collision.gameObject.tag == "Goal" && m_isWorking)
        {
            Debug.Log("Goal. Time: " + m_timer.ToString());
            ReportGoalTime();
            StopWorking();
        }
    }

    /// <summary>
    /// 動き始める。またタイムの計測を始める。
    /// </summary>
    public void StartWorking()
    {        
        m_isWorking = true;
    }

    /// <summary>
    /// 停止する。タイムの計測も止まる。
    /// </summary>
    public void StopWorking()
    {
        m_isWorking = false;
    }

    private void OnEnable()
    {
        m_camera.enabled = true;    // カメラで自分を写す
    }

    /// <summary>
    /// NetworkManager にゴールタイムを報告する
    /// </summary>
    void ReportGoalTime()
    {
        NetworkManager manager = GameObject.Find("GameManager").GetComponent<NetworkManager>();
        manager.ReportGoalTime(m_timer);
    }
}
